package ru.t1.rleonov.tm.configuration;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.rleonov.tm")
public class LoggerConfiguration {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

}
