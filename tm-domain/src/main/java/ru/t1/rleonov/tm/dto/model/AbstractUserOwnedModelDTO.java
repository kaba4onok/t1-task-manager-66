package ru.t1.rleonov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.model.IWBS;
import ru.t1.rleonov.tm.enumerated.Status;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractUserOwnedModelDTO extends AbstractModelDTO implements IWBS {

    @Nullable
    @Column(nullable = false, name = "user_id")
    protected String userId;

    @Column
    @NotNull
    protected String name = "";

    @Column
    @NotNull
    protected String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    protected Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return name + " : " + status + " : " + description;
    }

}
