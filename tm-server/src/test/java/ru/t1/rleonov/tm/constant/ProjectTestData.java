package ru.t1.rleonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.dto.model.ProjectDTO;
import java.util.ArrayList;
import java.util.List;
import static ru.t1.rleonov.tm.constant.UserTestData.*;

public final class ProjectTestData {

    @NotNull
    public static final ProjectDTO USER1_PROJECT1 = new ProjectDTO("USER1_PROJECT1", "USER1_DESC1");

    @NotNull
    public static final ProjectDTO USER1_PROJECT2 = new ProjectDTO("USER1_PROJECT2", "USER1_DESC2");

    @NotNull
    public static final ProjectDTO USER1_PROJECT3 = new ProjectDTO("USER1_PROJECT3", "USER1_DESC3");

    @NotNull
    public static final ProjectDTO USER1_PROJECT4 = new ProjectDTO("USER1_PROJECT4", "USER1_DESC4");

    @NotNull
    public static final ProjectDTO USER2_PROJECT1 = new ProjectDTO("USER2_PROJECT1", "USER2_DESC1");

    @NotNull
    public static final ProjectDTO USER2_PROJECT2 = new ProjectDTO("USER2_PROJECT2", "USER2_DESC2");

    @NotNull
    public static final ProjectDTO ADMIN_PROJECT1 = new ProjectDTO("ADMIN_PROJECT1", "ADMIN_DESC1");

    @NotNull
    public static final ProjectDTO ADMIN_PROJECT2 = new ProjectDTO("ADMIN_PROJECT2", "ADMIN_DESC2");

    @NotNull
    public static final ProjectDTO WRONG_USERID_PROJECT = new ProjectDTO("WRONG_USERID", "WRONG_USERID");

    @NotNull
    public static final List<ProjectDTO> USER1_PROJECTS = new ArrayList<>();

    @NotNull
    public static final List<ProjectDTO> USER2_PROJECTS = new ArrayList<>();

    @NotNull
    public static final List<ProjectDTO> ADMIN_PROJECTS = new ArrayList<>();

    @NotNull
    public static final List<ProjectDTO> ALL_PROJECTS = new ArrayList<>();

    static {
        USER1_PROJECTS.add(USER1_PROJECT1);
        USER1_PROJECTS.add(USER1_PROJECT2);
        USER1_PROJECTS.add(USER1_PROJECT3);

        USER2_PROJECTS.add(USER2_PROJECT1);
        USER2_PROJECTS.add(USER2_PROJECT2);

        ADMIN_PROJECTS.add(ADMIN_PROJECT1);
        ADMIN_PROJECTS.add(ADMIN_PROJECT2);

        USER1_PROJECT1.setStatus(Status.COMPLETED);
        WRONG_USERID_PROJECT.setUserId("!!!WRONG_USERID!!!");

        USER1_PROJECTS.forEach(project -> project.setUserId(USER1.getId()));
        USER2_PROJECTS.forEach(project -> project.setUserId(USER2.getId()));
        ADMIN_PROJECTS.forEach(project -> project.setUserId(ADMIN.getId()));

        ALL_PROJECTS.addAll(USER1_PROJECTS);
        ALL_PROJECTS.addAll(USER2_PROJECTS);
        ALL_PROJECTS.addAll(ADMIN_PROJECTS);
    }

}
