package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @Nullable
    UserDTO findFirstByLogin(@Nullable final String login);

    @Nullable
    UserDTO findFirstByEmail(@Nullable final String email);

}
