package ru.t1.rleonov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.rleonov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.rleonov.tm.api.service.IPropertyService;
import ru.t1.rleonov.tm.dto.request.ServerAboutRequest;
import ru.t1.rleonov.tm.dto.request.ServerVersionRequest;
import ru.t1.rleonov.tm.dto.response.ServerAboutResponse;
import ru.t1.rleonov.tm.dto.response.ServerVersionResponse;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.rleonov.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @Override
    @NotNull
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @Override
    @NotNull
    @WebMethod
    public String getHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }

}
