package ru.t1.rleonov.tm.api.service.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.List;

public interface IService<M> {

    @NotNull
    List<M> findAll();

    @NotNull
    M findById(@Nullable String id);

    @NotNull
    void deleteById(@Nullable String id);

}
