package ru.t1.rleonov.tm.repository.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.web.TaskWeb;

@Repository
public interface TaskWebRepository extends JpaRepository<TaskWeb, String> {

    void deleteByProjectId(@NotNull final String projectId);

}
