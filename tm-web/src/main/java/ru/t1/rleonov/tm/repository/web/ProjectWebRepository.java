package ru.t1.rleonov.tm.repository.web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.web.ProjectWeb;

@Repository
public interface ProjectWebRepository extends JpaRepository<ProjectWeb, String> {

}
