package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.model.dto.ProjectDTO;
import java.util.List;

@Repository
public interface ProjectDTORepository extends JpaRepository<ProjectDTO, String> {

    void deleteByUserId(@NotNull String userId);

    @Nullable
    ProjectDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}