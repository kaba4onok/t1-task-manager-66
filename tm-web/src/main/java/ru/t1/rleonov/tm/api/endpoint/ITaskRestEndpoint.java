package ru.t1.rleonov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import java.util.List;

public interface ITaskRestEndpoint {

    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @PostMapping("/save")
    TaskDTO save(@RequestBody TaskDTO task);

    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody TaskDTO task);

    @PostMapping("/deleteAll")
    void clear(@RequestBody List<TaskDTO> tasks);

    @PostMapping("/clear")
    void clear();

}
