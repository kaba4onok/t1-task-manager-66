package ru.t1.rleonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.rleonov.tm.client.TaskEndpointClient;
import ru.t1.rleonov.tm.marker.IntegrationCategory;
import ru.t1.rleonov.tm.model.dto.TaskDTO;
import java.util.List;
import static ru.t1.rleonov.tm.constant.TaskTestData.*;

public class TaskEndpointTest {

    @NotNull
    static final TaskEndpointClient client = TaskEndpointClient.client();

    @BeforeClass
    public static void setUpClass() {
        client.clear();
    }

    @Before
    public void setUp() {
        client.save(TASK1);
        client.save(TASK2);
        client.save(TASK3);
    }

    @After
    public void reset() {
        client.clear();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        @NotNull final List<TaskDTO> tasks = client.findAll();
        Assert.assertEquals(TASKS.size(), tasks.size());
        Assert.assertEquals(TASKS.get(0).getId(), tasks.get(0).getId());
        Assert.assertEquals(TASKS.get(1).getId(), tasks.get(1).getId());
        Assert.assertEquals(TASKS.get(2).getId(), tasks.get(2).getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void save() {
        @NotNull final TaskDTO task = client.save(TASK_NEW);
        Assert.assertEquals(task.getId(), TASK_NEW.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        @NotNull final String id = TASK1.getId();
        @NotNull final TaskDTO task = client.findById(id);
        Assert.assertEquals(task.getId(), id);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void existsById() {
        Assert.assertTrue(client.existsById(TASK1.getId()));
        Assert.assertFalse(client.existsById(TASK_NEW.getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        Assert.assertEquals(client.count(), TASKS.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteById() {
        @NotNull final String id = TASK1.getId();
        Assert.assertNotNull(client.findById(id));
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void delete() {
        @NotNull final String id = TASK1.getId();
        Assert.assertNotNull(client.findById(id));
        client.delete(TASK1);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void deleteAll() {
        Assert.assertEquals(client.findAll().size(), TASKS.size());
        client.save(TASK_NEW);
        client.clear(TASKS);
        Assert.assertEquals(client.findAll().size(), 1);
        Assert.assertEquals(client.findAll().get(0).getId(), TASK_NEW.getId());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void clear() {
        Assert.assertEquals(client.findAll().size(), TASKS.size());
        client.clear(TASKS);
        Assert.assertEquals(client.findAll().size(), 0);
    }

}
